<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //Membuat role admin
      $adminRole = new Role();
      $adminRole->name = "admin";
      $adminRole->display_name = "Admin";
      $adminRole->save();

      //Membuat role Penyelenggara
      $penyelenggaraRole = new  Role();
      $penyelenggaraRole->name = "penyelenggara";
      $penyelenggaraRole->display_name = "Penyelenggara";
      $penyelenggaraRole->save();

      //Membuat role Penyeleksi
      $penyeleksiRole = new  Role();
      $penyeleksiRole->name = "penyeleksi";
      $penyeleksiRole->display_name = "Penyeleksi";
      $penyeleksiRole->save();

      //Membuat role Peserta
      $pesertaRole = new  Role();
      $pesertaRole->name = "peserta";
      $pesertaRole->display_name = "Peserta";
      $pesertaRole->save();

      //Membuat sample admin
      $admin = new User();
      $admin->name = 'Administrator';
      $admin->email = 'admin@sipmalun.com';
      $admin->password = bcrypt('rahasia');
      $admin->save();
      $admin->attachRole($adminRole);

      //Membuat sample penyelenggara
      $penyelenggara = new User();
      $penyelenggara->name = "Penyelenggara Sipmalun";
      $penyelenggara->email = "penyelenggara@sipmalun.com";
      $penyelenggara->password = bcrypt('rahasia');
      $penyelenggara->save();
      $penyelenggara->attachRole($penyelenggaraRole);

      //Membuat sample penyeleksi
      $penyeleksi = new User();
      $penyeleksi->name = "Penyeleksi Sipmalun";
      $penyeleksi->email = "penyeleksi@sipmalun.com";
      $penyeleksi->password = bcrypt('rahasia');
      $penyeleksi->save();
      $penyeleksi->attachRole($penyeleksiRole);

      //Membuat sample penyeleksi
      $peserta = new User();
      $peserta->name = "Peserta Sipmalun";
      $peserta->email = "peserta@sipmalun.com";
      $peserta->password = bcrypt('rahasia');
      $peserta->save();
      $peserta->attachRole($pesertaRole);
    }
}
